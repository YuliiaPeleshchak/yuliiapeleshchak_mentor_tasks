def chek_data_type(type_valid):
    """
    This decorator checks if function arguments have valid type.

    :param type_valid: list of valid types
    :return:
    """

    def decorator(func):
        def wrapped_func(*args, **kwargs):
            if not isinstance(args[1], type_valid):
                raise TypeError("Invalid data type.")
            return func(*args, **kwargs)

        return wrapped_func

    return decorator


def check_list_element_type(type_valid):
    """
       This decorator checks if function list argument contains valid elements..

       :param type_valid: list of valid types
       :return:
       """

    def decorator(func):
        def wrapped_func(*args, **kwargs):
            for element in args[1]:
                if not isinstance(element, type_valid):
                    raise TypeError("Invalid data type.")
            return func(*args, **kwargs)

        return wrapped_func

    return decorator
