import logging
from unittest import TestCase

from task7_usersCV.config import USERS_CV_TEST_PATH
from task7_usersCV.database_connector import Database
from task7_usersCV.file_parser import FileParser
from task7_usersCV.user_model import User


logging.disable(logging.INFO)
logging.disable(logging.ERROR)


class TestIntegration(TestCase):
    """
    TestIntegration provides integration test for project.

    """

    @classmethod
    def setUpClass(cls):
        cls.path = USERS_CV_TEST_PATH
        cls.invalid_path = "invalid_path"
        cls.result = [
            {
                "FirstName": "David",
                "LastName": "David",
                "Email": "my_test2@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 1",
                "Position": "Developer",
            },
            {
                "FirstName": "Mark",
                "LastName": "Mark",
                "Email": "my_test3@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 2",
                "Position": "Developer",
            },
            {
                "FirstName": "Andrii",
                "LastName": "Andrii",
                "Email": "my_test1@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 1",
                "Position": "Developer",
            },
        ]

    def setUp(self):
        self.database = Database()
        self.file_parser_valid = FileParser(self.path)
        self.file_parser_invalid = FileParser(self.invalid_path)

    def tearDown(self):
        self.database.session.query(User).delete()
        self.database.session.commit()

    def test_parse_and_save_to_db_success(self):
        parsed_users = self.file_parser_valid.parse()
        self.database.save_all(parsed_users)
        users_from_db = self.database.get_all_users()
        self.assertEqual(len(users_from_db), 3)
        emails = [user.email for user in users_from_db]
        for user in self.result:
            self.assertIn(user["Email"], emails)

    def test_parse_and_save_to_db_fail(self):
        with self.assertRaises(FileNotFoundError) as error:
            self.database.save_all(self.file_parser_invalid.parse())
        self.assertEqual(error.exception.args[0], "There are not files")
        self.assertEqual(self.database.get_all_users(), [])
