import logging
from unittest import TestCase

from task7_usersCV.database_connector import Database
from task7_usersCV.user_model import User


logging.disable(logging.INFO)
logging.disable(logging.ERROR)


class TestFileParser(TestCase):
    """
    TestUserModel provides test methods for class UserModel.

    """

    def setUp(self):
        self.data = {
            "FirstName": "David",
            "LastName": "Doe",
            "Email": "david_test@email.com",
            "Birth": "12.10.2000",
            "Address": "Lviv, street 1",
            "Position": "Developer",
        }
        self.user = User(self.data)
        self.database = Database()

    def tearDown(self):
        self.database.session.query(User).delete()
        self.database.session.commit()

    def test_save_and_delete_success(self):
        second_user_data = {
            "FirstName": "MARK",
            "LastName": "Doe",
            "Email": "second_test@email.com",
            "Birth": "12.10.1990",
            "Address": "Lviv, street 2",
            "Position": "Developer",
        }
        second_user = User(second_user_data)
        users = []
        users.append(self.data)
        users.append(second_user_data)
        self.database.save_all(users)
        self.database.delete_user(self.user.email)
        self.assertIsNone(
            self.database.session.query(User)
            .filter(User.email == self.user.email)
            .first()
        )
        self.assertEqual(
            str(
                self.database.session.query(User)
                .filter(User.email == second_user.email)
                .first()
            ),
            str(second_user),
        )

    def test_save_and_delete_fail(self):
        second_user_data = {
            "FirstName": "MARK",
            "LastName": "Doe",
            "Email": "second_test@email.com",
            "Birth": "12.10.1990",
            "Address": "Lviv, street 2",
            "Position": "Developer",
        }
        users = []
        users.append(second_user_data)
        self.database.save_all(users)
        self.assertEqual(self.database.delete_user(self.user.email), None)

    def test_save_update_and_delete_success(self):
        second_user_data = {
            "FirstName": "MARK",
            "LastName": "Doe",
            "Email": "second_test@email.com",
            "Birth": "12.10.1990",
            "Address": "Lviv, street 2",
            "Position": "Developer",
        }
        second_user = User(second_user_data)
        users = []
        users.append(self.data)
        users.append(second_user_data)
        self.database.save_all(users)

        user_from_db = (
            self.database.session.query(User)
            .filter(User.email == self.user.email)
            .first()
        )

        self.assertEqual(user_from_db.address, self.data["Address"])
        self.assertEqual(user_from_db.status, self.data["Position"])

        new_user_data = {
            "FirstName": "David",
            "LastName": "Doe",
            "Email": "david_test@email.com",
            "Birth": "12.10.2000",
            "Address": "Lviv, street 1",
            "Position": "Developer",
        }
        updated_user = User(new_user_data)

        self.database.save_or_update(updated_user)
        updated_user = (
            self.database.session.query(User)
            .filter(User.email == "david_test@email.com")
            .first()
        )

        self.assertEqual(updated_user.address, new_user_data["Address"])
        self.assertEqual(updated_user.status, new_user_data["Position"])

        self.database.delete_user(self.user.email)
        self.assertIsNone(
            self.database.session.query(User)
            .filter(User.email == self.user.email)
            .first()
        )
        self.assertEqual(
            str(
                self.database.session.query(User)
                .filter(User.email == second_user.email)
                .first()
            ),
            str(second_user),
        )
