import re
from datetime import datetime

from sqlalchemy import Column
from sqlalchemy import Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import validates

from logger import logger
from task7_usersCV.config import TABLE_NAME

NAME_FORMAT = r"(^[a-zA-Z+])"
EMAIL_FORMAT = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"


Base = declarative_base()


class User(Base):
    """
    The User class provides methods creating new user object or updating
    existing user.

    """

    __tablename__ = TABLE_NAME

    id = Column(Integer, primary_key=True, autoincrement=True)
    firstName = Column(String)
    lastName = Column(String)
    email = Column(String, unique=True)
    birth = Column(String)
    address = Column(String)
    status = Column(String)

    def __init__(self, data):
        self.pattern_email = re.compile(EMAIL_FORMAT)
        self.pattern_name = re.compile(NAME_FORMAT)
        self.firstName = data.get("FirstName")
        self.lastName = data.get("LastName")
        self.email = data.get("Email")
        self.birth = data.get("Birth")
        self.address = data.get("Address")
        self.status = data.get("Position")

    def __repr__(self):
        logger.info("User was created.")
        return "<User({}, {}, {})>".format(self.firstName, self.lastName, self.email)

    def get_full_name(self):
        """
         This method returns user full name.

         :return: String with user full name
         """
        return "{} {}".format(self.firstName, self.lastName)

    @validates("firstName", "lastName")
    def validate_name(self, key, value):
        """
         This method validates user firstName and lastName.
         Raise ValueError, if firstName or lastName is invalid.

         :param key:
         :param value: firstName or lastName

         :return: firstName or lastName
         """
        if value:
            if not self.pattern_name.match(value):
                logger.error("Error.Invalid name.")
                raise ValueError("Error.Invalid name.")
            return value
        return None

    @validates("email")
    def validate_email(self, key, email):
        """
        This method validates user email.
        Raise ValueError, if email is invalid.

        :param key:
        :param value: email

        :return: email
        """
        if email:
            if not self.pattern_email.match(email):
                logger.error("Error.Invalid email.")
                raise ValueError("Error.Invalid email.")
            return email
        return None

    @validates("birth")
    def validate_date(self, key, date):
        """
        This method validates user data of birth.
        Raise ValueError, if data of birth is invalid.

        :param key:
        :param value: data of birth.

        :return: data of birth.
        """
        if date:
            try:
                datetime.strptime(date, "%d.%m.%Y").strftime("%d.%m.%Y")
            except ValueError:
                logger.error("Error.Invalid date of birth.")
                raise ValueError("Error.Invalid date of birth.")
            return date
        return None

    def update(self, data):
        """
        This method updates user data (address or position)

        :param data: Data, which we want to update

        :return:
        """
        self.address = data.get("Address", self.address)
        self.status = data.get("Position", self.status)
