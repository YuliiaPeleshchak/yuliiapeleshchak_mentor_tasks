import glob
import os
import re
from threading import Thread

from tqdm import tqdm

from logger import logger


class FileParser(object):
    """
    The FileParser class provides methods for parsing users CV data
    from files with .txt extension and save these to database.
   """

    def __init__(self, path):
        self.path = path
        self.logger = logger

    def get_list_of_files(self):
        """
       This method returns list of files from path.
       Raises FileNotFoundError if there are not files.

       :return: List of files
       """
        files = glob.glob(os.path.join(self.path, "*.txt"))
        if len(files) == 0:
            self.logger.error("There are not files")
            raise FileNotFoundError("There are not files")
        return files

    def read_from_file(self, filename, list_of_parsed_data, bar_data):
        """
        This method adds user CV data from file with .txt extension to list of
        parsed data.

        :param filename: Name of file, which we want to parse.
        :param list_of_parsed_data: List of parsed data
        :param bar_data: Tuple, that contains progress bar and process number.

        :return:
        """
        bar, process_number = bar_data
        bar.set_description(f"Bar for process: {process_number}")
        with open(filename, "r") as file:
            group = dict()
            for key, value in re.findall(r"(.*): (.*)", file.read()):
                group[key] = value
                bar.update(10)
            list_of_parsed_data.append(group)

    def parse(self):
        """
       This method parse users CV data from files with .txt extension.
       Each of files will be parsed in separate Thread.

       :return: List of dictionary with data from users CV
       """

        list_of_parsed_data = []
        files = self.get_list_of_files()
        threads = []
        files_len = len(files)
        bars = [tqdm(total=10 * files_len) for i in range(files_len)]
        process_number = 0
        for filename in files:
            process = Thread(
                target=self.read_from_file,
                args=[
                    filename,
                    list_of_parsed_data,
                    (bars[process_number], process_number),
                ],
            )
            process_number += 1
            process.start()
            threads.append(process)
        for process in threads:
            process.join()
        self.logger.info("Users CV were parsed.")
        return list_of_parsed_data
