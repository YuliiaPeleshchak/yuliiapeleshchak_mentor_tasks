from os import getcwd

DATABASE = "postgresql"
USER_NAME = "postgres"
USER_PASSWORD = "postgres"
HOST = "localhost"

# TABLE_NAME = "users"

# Data for test
TABLE_NAME = "mentor_task"

# Path to directory with users CV for testing task7
USERS_CV_TEST_PATH = getcwd() + "/task7_usersCV/tests/test_users"
