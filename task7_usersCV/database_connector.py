import sqlalchemy as db
from sqlalchemy.orm import Session

from logger import logger
from task7_usersCV.config import DATABASE, USER_NAME, USER_PASSWORD, HOST, TABLE_NAME
from task7_usersCV.user_model import User, Base


class Database(object):
    """
    The Database class provides methods for connecting to database, saving or
    updating data, deleting data and getting all data.
    """

    engine = db.create_engine(
        "{}://{}:{}@{}/{}".format(DATABASE, USER_NAME, USER_PASSWORD, HOST, TABLE_NAME)
    )

    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.instance = super(Database, cls).__new__(cls)
        cls.logger = logger
        return cls.instance

    def __init__(self):
        if not self.engine.dialect.has_table(self.engine, TABLE_NAME):
            Base.metadata.create_all(self.engine)
        self.connection = self.engine.connect()
        self.session = Session(bind=self.connection)

        self.log = logger

    def save_or_update(self, data):
        """
        This methods saves new data to database or updates data of existing user.

        :param data: Data, which we want to save or update

        :return:
        """
        user = self.session.query(User).filter(User.email == data.email).first()
        if user:
            dataToUpdate = {"Address": data.address, "Status": data.status}
            user.update(dataToUpdate)
            self.log.info("User'{}' was updated.".format(user))
        else:
            self.session.add(data)
            self.log.info("New user was added")
        self.session.commit()

    def save_all(self, users):
        """
       This method saves users CV data to database.

       :return:
       """
        if users:
            for user in users:
                self.save_or_update(User(user))
                self.logger.info("User with data {} was saved to database".format(user))
        else:
            self.logger.error("There are not new users.")

    def get_all_users(self):
        """
        This method returns all users from database.

        :return: List of user objects
        """
        self.session = self.session
        users = self.session.query(User).all()
        return users

    def delete_user(self, user_email):
        """
        This method deletes user from database by his email.

        :param user_email: User email

        :return:
        """
        session = self.session
        user = session.query(User).filter(User.email == user_email).first()
        if user:
            session.delete(user)
            self.log.info("User'{}' was deleted.".format(user))
            session.commit()
        else:
            self.log.error("User not found.")
