import logging
from unittest import TestCase

from task7_usersCV.user_model import User


logging.disable(logging.INFO)
logging.disable(logging.ERROR)


class TestUserModel(TestCase):
    """
    TestUserModel provides unit test methods for class UserModel.
    """

    @classmethod
    def setUpClass(cls):
        super(TestUserModel, cls).setUpClass()
        cls.firstName = "David"
        cls.lastName = "Doe"
        cls.email = "david_test@email.com"
        cls.birth = "12.10.2000"
        cls.address = "Lviv, street 1"
        cls.status = "Developer"

    def test_create_success(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        User(data)

    def test_create_success_with_none_name(self):
        data = {
            "FirstName": "",
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.firstName, None)

    def test_create_success_with_none_email(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": "",
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.email, None)

    def test_create_success_with_none_birth(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": "",
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.birth, None)

    def test_create_success_with_none_address(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": "",
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.address, "")

    def test_create_success_with_none_position(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": "",
        }
        user = User(data)
        self.assertEqual(user.status, "")

    def test_create_fail_first_name(self):
        data = {
            "FirstName": "213",
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        with self.assertRaises(ValueError) as error:
            User(data)
        self.assertEqual(error.exception.args[0], "Error.Invalid name.")

    def test_create_fail_last_name(self):
        data = {
            "FirstName": self.lastName,
            "LastName": "213",
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        with self.assertRaises(ValueError) as error:
            User(data)
        self.assertEqual(error.exception.args[0], "Error.Invalid name.")

    def test_create_fail_email(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": "wrong_email",
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        with self.assertRaises(ValueError) as error:
            User(data)
        self.assertEqual(error.exception.args[0], "Error.Invalid email.")

    def test_create_fail_name(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": "111",
            "Address": self.address,
            "Position": self.status,
        }
        with self.assertRaises(ValueError) as error:
            User(data)
        self.assertEqual(error.exception.args[0], "Error.Invalid date of birth.")

    def test_update_one_field(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.address, self.address)

        new_data = {"Address": "new_addr"}
        user.update(new_data)
        self.assertEqual(user.address, new_data["Address"])

    def test_update_two_fields(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(user.address, self.address)
        self.assertEqual(user.status, self.status)

        new_data = {"Address": "new_addr", "Position": "new_position"}
        user.update(new_data)
        self.assertEqual(user.address, new_data["Address"])
        self.assertEqual(user.status, new_data["Position"])

    def test_get_full_name(self):
        data = {
            "FirstName": self.firstName,
            "LastName": self.lastName,
            "Email": self.email,
            "Birth": self.birth,
            "Address": self.address,
            "Position": self.status,
        }
        user = User(data)
        self.assertEqual(
            user.get_full_name(), "{} {}".format(self.firstName, self.lastName)
        )
