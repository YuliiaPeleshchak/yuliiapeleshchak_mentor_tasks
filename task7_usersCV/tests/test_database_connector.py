import logging
from unittest import TestCase

from task7_usersCV.database_connector import Database
from task7_usersCV.user_model import User


logging.disable(logging.INFO)
logging.disable(logging.ERROR)


class TestDatabaseConnector(TestCase):
    """
    TestDatabaseConnector provides unit test methods for class DatabaseConnector.

    """

    def setUp(self):
        self.data = {
            "FirstName": "David",
            "LastName": "Doe",
            "Email": "david_test@email.com",
            "Birth": "12.10.2000",
            "Address": "Lviv, street 1",
            "Position": "Developer",
        }
        self.user = User(self.data)
        self.database = Database()

    def tearDown(self):
        self.database.session.query(User).delete()
        self.database.session.commit()

    def test_save(self):
        self.database.save_or_update(self.user)
        self.assertEqual(
            str(
                self.database.session.query(User)
                .filter(User.email == self.user.email)
                .first()
            ),
            str(self.user),
        )

    def test_save_to_database_success(self):
        second_user_data = {
            "FirstName": "MARK",
            "LastName": "Doe",
            "Email": "second_test@email.com",
            "Birth": "12.10.1990",
            "Address": "Lviv, street 2",
            "Position": "Developer",
        }
        new_user = User(second_user_data)
        users = []
        users.append(self.data)
        users.append(second_user_data)
        self.database.save_all(users)
        self.assertEqual(
            str(
                self.database.session.query(User)
                .filter(User.email == self.user.email)
                .first()
            ),
            str(self.user),
        )
        self.assertEqual(
            str(
                self.database.session.query(User)
                .filter(User.email == new_user.email)
                .first()
            ),
            str(new_user),
        )

    def test_save_to_database_fails(self):
        users = None
        self.assertEqual(self.database.save_all(users), None)

    def test_update(self):
        self.database.save_or_update(self.user)
        user = (
            self.database.session.query(User)
            .filter(User.email == self.user.email)
            .first()
        )

        self.assertEqual(user.address, self.data["Address"])
        self.assertEqual(user.status, self.data["Position"])

        new_user_data = {
            "FirstName": "David",
            "LastName": "Doe",
            "Email": "david_test@email.com",
            "Birth": "12.10.2000",
            "Address": "Lviv, street 1",
            "Position": "Developer",
        }
        updated_user = User(new_user_data)

        self.database.save_or_update(updated_user)
        updated_user = (
            self.database.session.query(User)
            .filter(User.email == "david_test@email.com")
            .first()
        )

        self.assertEqual(updated_user.address, new_user_data["Address"])
        self.assertEqual(updated_user.status, new_user_data["Position"])

    def test_delete_user(self):
        self.database.save_or_update(self.user)
        self.database.delete_user(self.user.email)
        self.assertEqual(
            self.database.session.query(User)
            .filter(User.email == self.user.email)
            .first(),
            None,
        )

    def test_delete_fail(self):
        self.database.save_or_update(self.user)
        self.assertEqual(
            self.database.session.query(User)
            .filter(User.email == "test_email")
            .first(),
            None,
        )
        self.assertEqual(self.database.delete_user("test_email"), None)
