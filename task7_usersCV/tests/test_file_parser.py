import logging
from unittest import TestCase

from task7_usersCV.config import USERS_CV_TEST_PATH
from task7_usersCV.file_parser import FileParser


logging.disable(logging.INFO)
logging.disable(logging.ERROR)


class TestFileParser(TestCase):
    """
    TestFileParser provides unit test methods for class FileParser.

    """

    @classmethod
    def setUpClass(cls):
        cls.path = USERS_CV_TEST_PATH
        cls.invalid_path = "invalid_path"
        cls.result = [
            {
                "FirstName": "David",
                "LastName": "David",
                "Email": "my_test2@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 1",
                "Position": "Developer",
            },
            {
                "FirstName": "Mark",
                "LastName": "Mark",
                "Email": "my_test3@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 1",
                "Position": "Developer",
            },
            {
                "FirstName": "Mark",
                "LastName": "Mark",
                "Email": "my_test3@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 2",
                "Position": "Developer",
            },
            {
                "FirstName": "Andrii",
                "LastName": "Andrii",
                "Email": "my_test1@my.ua",
                "Birth": "01.01.2010",
                "Address": "Lviv , street 1",
                "Position": "Developer",
            },
        ]

    def setUp(self):
        self.file_parser_valid = FileParser(self.path)
        self.file_parser_invalid = FileParser(self.invalid_path)

    def test_get_files_success(self):
        files = [
            self.path + "/user2.txt",
            self.path + "/user3.txt",
            self.path + "/user4.txt",
            self.path + "/user1.txt",
        ]
        self.assertEqual(self.file_parser_valid.get_list_of_files(), files)

    def test_get_files_fails(self):
        with self.assertRaises(FileNotFoundError) as error:
            self.file_parser_invalid.get_list_of_files()
        self.assertEqual(error.exception.args[0], "There are not files")

    def test_parse_files_success(self):
        result = self.file_parser_valid.parse()
        for user_data in self.result:
            self.assertIn(user_data, result)
