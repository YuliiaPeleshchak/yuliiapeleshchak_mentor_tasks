import re

from utils import chek_data_type


PATTERN = "[A-Za-z0-9_]+"


class StringMatch(object):
    """
    The StringMatch provides method to match a string that contains only upper
    and lowercase letters, numbers, and underscores.
    """

    def __init__(self, logger):
        self.log = logger
        self.pattern = re.compile(PATTERN)

    @chek_data_type((str,))
    def find(self, string):
        """
        This method finds a matched string.

        :param string: String in which we will be find a matched string.
        :type: str
        :return: List which contains matched strings.
        """
        if not len(string):
            self.log.info("No string.")
            return None
        result = self.pattern.findall(string)
        if len(result):
            self.log.info(
                "List of strings that contains only upper and lowercase "
                "letters, numbers, and underscores: '{}'".format(result)
            )
            return result
        else:
            self.log.info("No matches.")
            return None
