import logging
from unittest import TestCase

from logger import logger
from task3_match_string.string_match import StringMatch


class TestStringMatch(TestCase):
    """
    TestStringMatch provides test methods for class StringMatch.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.obj = StringMatch(self.log)

    def test_find_success(self):
        string = "133^&3Noah_Emma2**LiamO_*livia63Mason@Sophia4JacobIsabell"
        expected_result = [
            "133",
            "3Noah_Emma2",
            "LiamO_",
            "livia63Mason",
            "Sophia4JacobIsabell",
        ]
        self.assertEqual(self.obj.find(string), expected_result)

    def test_find_fail_no_matches(self):
        string = "%^&#@!@$%^"
        self.assertIsNone(self.obj.find(string))

    def test_find_fail_no_string(self):
        string = ""
        self.assertIsNone(self.obj.find(string))

    def test_find_invalid_type(self):
        string = []
        with self.assertRaises(TypeError) as error:
            self.obj.find(string)
        self.assertEqual(error.exception.args[0], "Invalid data type.")
