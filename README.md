This is test repository for Python Internature.

What's Here
-----------

This repository contains solutions of several tasks and tests for each of them.
Tasks:

    1. Write a Python program to create a doubly linked list, append some items and iterate through the list, add delete, and search options

    2. Write a Python program for binary search.  

    3. Write a Python program to match a string that contains only upper and lowercase letters, numbers, and underscores.

    4. Write a Python program of recursion list sum.  

    5. Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included) and the values are square of keys. 

    6. Write a Python program to sort a list of dictionaries using Lambda.
    
    7. Write a program to parse User CV from file and store into database.



Getting Started
---------------
To work on the sample code, you'll need to clone project's repository to your
local computer.

To run tests you should do this command: python -m unittest <module-name>
 
Requirements for task7
---------------
To work with this task, you'll need to install modules from requirements.txt and change data to your own in config.py
