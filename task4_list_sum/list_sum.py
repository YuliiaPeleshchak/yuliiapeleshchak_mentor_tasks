from utils import chek_data_type, check_list_element_type


class ListSum(object):
    """
    The ListSum class provides method of of recursion counting list sum.
    """

    def __init__(self, logger):
        self.log = logger

    @chek_data_type((list,))
    @check_list_element_type((list, int, float))
    def list_sum(self, data):
        """
         Function of recursion counting list sum.

        :param data: Array for which count sum.
        :type: list
        :return: Sum of elements in array.
        """
        sum = 0
        for element in data:
            if type(element) == list:
                self.log.info("New recursion.")
                sum = sum + self.list_sum(element)
                self.log.info("Sum after recursion = {}".format(sum))
            else:
                sum += element
                self.log.info("Sum = {}".format(sum))
        return sum
