import logging
from unittest import TestCase

from logger import logger
from task4_list_sum.list_sum import ListSum


class TestListSum(TestCase):
    """
    TestListSum provides test methods for testing list_sum function.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.obj = ListSum(self.log)

    def test_list_sum_success(self):
        expected_result = 28
        test_array_first = [1, 2, [3, 4], [5, 6, [7]]]
        test_array_second = [1, 2, [3, [], 4], [5, 6, [7]]]
        self.assertEqual(self.obj.list_sum(test_array_first), expected_result)
        self.assertEqual(self.obj.list_sum(test_array_second), expected_result)

    def test_list_sum_fail_data_type(self):
        test_array = 1
        with self.assertRaises(TypeError) as error:
            self.obj.list_sum(test_array)
        self.assertEqual(error.exception.args[0], "Invalid data type.")

    def test_list_sum_fail_element_type(self):
        test_array = [1, 2, [3, ["f"], 4], [5, 6, [7]]]
        with self.assertRaises(TypeError) as error:
            self.obj.list_sum(test_array)
        self.assertEqual(error.exception.args[0], "Invalid data type.")
