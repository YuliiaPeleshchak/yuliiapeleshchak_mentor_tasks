from utils import chek_data_type


class LambdaSort(object):
    """
       The LambdaSort class provides method o sort a list of dictionaries
       using Lambda.
    """

    def __init__(self, logger):
        self.log = logger
        self.data = []

    @chek_data_type((dict,))
    def add_dict_to_list(self, data):
        """
        This method adds new dict to your data.

        :param data: dictionary which will be added to your data list
        :return:
        """

        self.data.append(data)
        self.log.info("Dict was added.")

    def delete_dict_from_list(self, position):
        """
        This method deletes dictionary by its index position in list.

        :param position: position dictionary in list
        :return:
        """
        if len(self.data) < position or position <= 0:
            self.log.error("Invalid position.")
            raise IndexError("Invalid position.")
        self.log.info("Dict {} was deleted.".format(self.data[position - 1]))
        self.data.pop((position - 1))

    def sort(self, key):
        """
        This method sorts dictionaries by target key.

        :param key: dictionaries will be sorted by this key
        :return:
        """
        for item in self.data:
            if key not in item.keys():
                self.log.error("Invalid key.")
                raise TypeError("Invalid key.")
        self.data = sorted(self.data, key=lambda dictionary: dictionary[key])
        self.log.info("Dictionaries were sorted.")
