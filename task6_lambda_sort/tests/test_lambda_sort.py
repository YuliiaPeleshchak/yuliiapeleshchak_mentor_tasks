import logging
from unittest import TestCase

from logger import logger
from task6_lambda_sort.lambda_sort import LambdaSort


class TestLambdaSort(TestCase):
    """
    TestLambdaSort provides test methods for class LambdaSort.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.data = LambdaSort(self.log)
        self.dict_1 = {"param1": 4, "param2": 5, "param3": 1}
        self.dict_2 = {"param1": 3, "param2": 1, "param3": 2}
        self.dict_3 = {"param1": 1, "param2": 1, "param3": 5}
        self.dicts = (self.dict_1, self.dict_2, self.dict_3)
        list(map(self.data.add_dict_to_list, self.dicts))

    def test_append_success(self):
        new_dict = {"param1": 43, "param2": 1, "param3": 1}
        expected_data = [
            {"param1": 4, "param2": 5, "param3": 1},
            {"param1": 3, "param2": 1, "param3": 2},
            {"param1": 1, "param2": 1, "param3": 5},
            {"param1": 43, "param2": 1, "param3": 1},
        ]
        self.data.add_dict_to_list(new_dict)
        self.assertEqual(self.data.data, expected_data)

    def test_append_fail(self):
        new_dict = 8
        with self.assertRaises(TypeError) as error:
            self.data.add_dict_to_list(new_dict)
        self.assertEqual(error.exception.args[0], "Invalid data type.")

    def test_sort_success(self):
        key = "param1"
        expected_data = [
            {"param1": 1, "param2": 1, "param3": 5},
            {"param1": 3, "param2": 1, "param3": 2},
            {"param1": 4, "param2": 5, "param3": 1},
        ]
        self.data.sort(key)
        self.assertEqual(self.data.data, expected_data)

    def test_sort_fail(self):
        key = 8
        with self.assertRaises(TypeError) as error:
            self.data.sort(key)
        self.assertEqual(error.exception.args[0], "Invalid key.")

    def test_delete_success(self):
        position = 3
        expected_data = [
            {"param1": 4, "param2": 5, "param3": 1},
            {"param1": 3, "param2": 1, "param3": 2},
        ]
        self.data.delete_dict_from_list(position)
        self.assertEqual(self.data.data, expected_data)

    def test_delete_fail(self):
        position_1 = 10
        position_2 = 0
        with self.assertRaises(IndexError) as error:
            self.data.delete_dict_from_list(position_1)
        self.assertEqual(error.exception.args[0], "Invalid position.")
        with self.assertRaises(IndexError) as error:
            self.data.delete_dict_from_list(position_2)
        self.assertEqual(error.exception.args[0], "Invalid position.")
