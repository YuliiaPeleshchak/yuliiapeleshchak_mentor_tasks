import logging
import random
from unittest import TestCase

from logger import logger
from task2_binary_search.binary_search import BinarySearch


class TestBinarySearch(TestCase):
    """
    TestBinarySearch provides test methods for testing program of binarySearch.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.array = [random.randint(10, 100) for iter in range(20)]
        self.obj = BinarySearch(self.array, self.log)

    def test_binary_search_return_true(self):
        value_to_found = self.array[5]
        self.assertTrue(self.obj.binary_search(value_to_found))

    def test_binary_search_return_false(self):
        value_to_found = 1
        self.assertFalse(self.obj.binary_search(value_to_found))

    def test_binary_search_fail_incorrect_value_type(self):
        incorrect_value_to_found = "5"
        with self.assertRaises(TypeError) as error:
            self.obj.binary_search(incorrect_value_to_found)
        self.assertEqual(error.exception.args[0], "Invalid data type.")

    def test_binary_search_fail_incorrect_array_type(self):
        incorrect_array = 9
        with self.assertRaises(TypeError) as error:
            BinarySearch(incorrect_array, self.log)
        self.assertEqual(error.exception.args[0], "Invalid data type.")

    def test_binary_search_fail_incorrect_array_element_type(self):
        incorrect_array = [3, 5, 6, "6"]
        with self.assertRaises(TypeError) as error:
            BinarySearch(incorrect_array, self.log)
        self.assertEqual(error.exception.args[0], "Invalid data type.")
