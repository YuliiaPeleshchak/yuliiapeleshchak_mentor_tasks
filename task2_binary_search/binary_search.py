from utils import chek_data_type, check_list_element_type


class BinarySearch(object):
    """
    The BinarySearch class provides method of searching target value in sorted
    array by binary search algorithm.

   """

    @chek_data_type((list,))
    @check_list_element_type((int, float))
    def __init__(self, array, logger):
        self.log = logger
        self.array = sorted(array)

    @chek_data_type((int, float))
    def binary_search(self, value):
        """
        This function finds if the sorted array contains a target value.

        :param array: Sorted array in which function finds a target value.
        :param value:  The target value which function finds in array.
        :return: True if value is in array else False
        """

        left = 0
        right = len(self.array) - 1
        while left <= right:
            self.log.info("Array is divided.")
            middle = left + (right - left) // 2

            if self.array[middle] == value:
                self.log.info("Value is founded.")
                return True

            elif self.array[middle] < value:
                left = middle + 1
                self.log.info(f"Array right: {self.array[left: right]}.")
            else:
                right = middle - 1
                self.log.info(f"Array right: {self.array[left: right + 1]}.")
        self.log.info("Value is not founded.")
        return False
