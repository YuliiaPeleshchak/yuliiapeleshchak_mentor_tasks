class Node(object):
    """
    The Node class creates new node for doubly linked list.
    """

    def __init__(self, data, next, previous):
        self.data = data
        self.next = next
        self.previous = previous


class DoublyLinkedList(object):
    """
    The DoublyLinkedList class creates doubly linked list with options:
    add element, delete element, delete element on special postition,
    search element.
   """

    def __init__(self, logger):
        self.head = None
        self.log = logger

    def __create_new_node(self, data, next=None, previous=None):
        """
        This private method creates new node.
        :param data: data of new node
        :param next: pointer to next element
        :param previous: pointer to previous element
        :return: new Node object
        """
        return Node(data, next, previous)

    def push(self, new_element):
        """
        This function adds element at the begin of list.

        :param new_element: element which will be added at the begin of the list
        :return:
        """

        newNode = self.__create_new_node(new_element, self.head)
        if self.head:
            self.head.previous = newNode
        self.head = newNode
        self.log.info("Element is added at the begin.")

    def insert(self, previous_node, new_element):
        """
        This function adds element at the special position.

        :param previous_node: pointer to previous node
        :param new_element: element which will be added at the target position
        :return: None if previous node is None
        """
        if not previous_node:
            return
        newNode = self.__create_new_node(new_element, previous_node.next)
        previous_node.next = newNode
        newNode.previous = previous_node
        if newNode.next:
            newNode.next.previous = newNode
        self.log.info("Element is added at this position.")

    def append(self, new_element):
        """
        This function adds element at the end of list.

        :param new_element: element which will be added at the end of list
        :return:
        """
        newNode = self.__create_new_node(new_element)
        if not self.head:
            newNode.previous = None
            self.head = newNode
            return
        last = self.head
        while last.next:
            last = last.next
        last.next = newNode
        newNode.previous = last
        self.log.info("Element is added at the end.")

    def list_print(self):
        """
        This function returns all elements of list.

        :return output: list of elements
        """

        output = []
        node = self.head
        while node:
            output.append(node.data)
            node = node.next
        self.log.info(f"list: {output}")
        return output

    def delete_value(self, value):
        """
        This function deletes element by its value.

        :param value: element with this value will be deleted
        :return:
        """

        node = self.head
        while node:
            if node.data == value:
                if node.previous and node.next:
                    node.previous.next = node.next
                    node.next.previous = node.previous
                    self.log.info("Element is deleted.")
                elif not node.previous and node.next:
                    self.head = node.next
                    node.next.previous = None
                    self.log.info("Element is deleted.")
                elif not node.previous and not node.next:
                    self.head = None
                    self.log.info("Element is deleted.")
                else:
                    node.previous.next = None
                    self.log.info("Element is deleted.")
            node = node.next

    def delete_at_position(self, position):
        """
        This function deletes element at special position.
        Raises exception if element position is off the list.

        :param position: element at this position will be deleted
        :return:
        """

        node = self.head
        for i in range(position - 1):
            if not node.next:
                self.log.error("Error. Position is off the list.")
                raise IndexError("Error. Position is off the list.")
            node = node.next
        if node.previous and node.next:
            node.previous.next = node.next
            node.next.previous = node.previous
        elif not node.previous and node.next:
            self.head = node.next
            node.next.previous = None
        elif not node.previous and not node.next:
            self.head = None
        else:
            node.previous.next = None
        self.log.info("Element is deleted.")

    def search_value(self, value):
        """
        This function searches value in list and return its position.
        Raises None if list does not contain a target element.

        :param value: value to search in list
        :return: value position if value exists, else None
        """

        node = self.head
        position = 0
        if not node:
            self.log.warning("Empty list")
            return
        while node:
            if node.data == value:
                self.log.info(
                    f"Value {value} is founded at the position: {position+1} ({position})"
                )
                return position + 1
            node = node.next
            position += 1
        self.log.info(f"Value {value} is not founded")
        return None
