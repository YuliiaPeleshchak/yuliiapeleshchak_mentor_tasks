import logging

from logger import logger
from unittest import TestCase

from task1_linked_list.linked_list import DoublyLinkedList


class TestDoublyLinkedList(TestCase):
    """
    TestDoublyLinkedList provides test methods for class DoublyLinkedList.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.doubly_linked_list = DoublyLinkedList(self.log)
        self.values = (1, 2, 3, 4, 5)
        list(map(self.doubly_linked_list.append, self.values))

    def test_push(self):
        expected_result = [14, 13, 12, 1, 2, 3, 4, 5]
        values = (12, 13, 14)
        list(map(self.doubly_linked_list.push, values))
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_append(self):
        expected_result = [1, 2, 3, 4, 5, 12, 13, 14]
        values = (12, 13, 14)
        list(map(self.doubly_linked_list.append, values))
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_insert_success(self):
        expected_result = [1, 2, 12, 3, 4, 5]
        self.doubly_linked_list.insert(self.doubly_linked_list.head.next, 12)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_insert_fail(self):
        expected_result = [1, 2, 3, 4, 5]
        self.doubly_linked_list.insert(self.doubly_linked_list.head.previous, 12)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_first_success(self):
        value_to_delete = 1
        expected_result = [2, 3, 4, 5]
        self.doubly_linked_list.delete_value(value_to_delete)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_middle_success(self):
        value_to_delete = 3
        expected_result = [1, 2, 4, 5]
        self.doubly_linked_list.delete_value(value_to_delete)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_last_success(self):
        value_to_delete = 5
        expected_result = [1, 2, 3, 4]
        self.doubly_linked_list.delete_value(value_to_delete)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_single_success(self):
        doubly_linked_list = DoublyLinkedList(self.log)
        doubly_linked_list.append(1)
        value_to_delete = 1
        expected_result = []
        doubly_linked_list.delete_value(value_to_delete)
        self.assertEqual(doubly_linked_list.list_print(), expected_result)

    def test_delete_value_fail(self):
        value_to_delete = 100
        expected_result = [1, 2, 3, 4, 5]
        self.doubly_linked_list.delete_value(value_to_delete)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_at_first_position_success(self):
        position = 1
        expected_result = [2, 3, 4, 5]
        self.doubly_linked_list.delete_at_position(position)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_at_second_position_success(self):
        position = 2
        expected_result = [1, 3, 4, 5]
        self.doubly_linked_list.delete_at_position(position)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_at_last_position_success(self):
        position = 5
        expected_result = [1, 2, 3, 4]
        self.doubly_linked_list.delete_at_position(position)
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_delete_value_single_at_position_success(self):
        doubly_linked_list = DoublyLinkedList(self.log)
        doubly_linked_list.append(8)
        position = 1
        expected_result = []
        doubly_linked_list.delete_at_position(position)
        self.assertEqual(doubly_linked_list.list_print(), expected_result)

    def test_delete_value_at_position_fail(self):
        position = 10
        expected_result = [1, 2, 3, 4, 5]
        with self.assertRaises(IndexError) as error:
            self.doubly_linked_list.delete_at_position(position)
        self.assertEqual(error.exception.args[0], "Error. Position is off the list.")
        self.assertEqual(self.doubly_linked_list.list_print(), expected_result)

    def test_search_success(self):
        value_to_find = 2
        expected_position = 2
        self.assertEqual(
            self.doubly_linked_list.search_value(value_to_find), expected_position
        )

    def test_search_fail_not_value_in_list(self):
        value_to_find = 100
        self.doubly_linked_list.search_value(value_to_find)
        self.assertIsNone(self.doubly_linked_list.search_value(value_to_find))

    def test_search_fail_empty_list(self):
        value_to_find = 100
        empty_list = DoublyLinkedList(self.log)
        empty_list.search_value(value_to_find)
        self.assertIsNone(empty_list.search_value(value_to_find))
