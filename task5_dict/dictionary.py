class Dictionary(object):
    """
    The Dictionary class provides method for creation dictionary,
    where the keys are numbers between special range and the values
    are square of keys.
    """

    def __init__(self, logger):
        self.log = logger

    def dictionary(self, *kwards):
        """
        This method decides range of dictionary creation.

        :param kwards: tuple of values for creating dictionary (from, to, step)
        or only one of this items
        :type: tuple
        :return: dict
        """

        if len(kwards) == 1:
            if not isinstance(kwards[0], int):
                self.log.error("Invalid data. Parameters must be integer type.")
                raise TypeError("Invalid data. Parameters must be integer type.")
            self.log.info("Range from {} to {} with step {}".format(1, kwards[0], 1))
            return self.__dictionary_range_one_parameters(kwards[0])
        elif len(kwards) == 2:
            if not all([isinstance(kwards[0], int), isinstance(kwards[1], int)]):
                self.log.error("Invalid data. Parameters must be integer type.")
                raise TypeError("Invalid data. Parameters must be integer type.")
            self.log.info(
                "Range from {} to {} with step {}".format(kwards[0], kwards[1] + 1, 1)
            )
            return self.__dictionary_range_two_parameters(kwards[0], kwards[1])
        elif len(kwards) == 3:
            if not all(
                [
                    isinstance(kwards[0], int),
                    isinstance(kwards[1], int),
                    isinstance(kwards[2], int),
                ]
            ):
                self.log.error("Invalid data. Parameters must be integer type.")
                raise TypeError("Invalid data. Parameters must be integer type.")
            self.log.info(
                "Range from {} to {} with step {}".format(
                    kwards[0], kwards[1] + 1, kwards[2]
                )
            )
            return self.__dictionary_range_three_parameters(
                kwards[0], kwards[1], kwards[2]
            )
        else:
            self.log.error(
                "Invalid data. You should pass one, two or three parameters."
            )
            raise Exception(
                "Invalid data. You should pass one, two or three parameters."
            )

    def __dictionary_range_one_parameters(self, stop):
        """
        :param stop: parameter 'to' for range()
        :type: int
        :return: dict
        """
        dict = {key: key ** 2 for key in range(1, stop + 1)}
        self.log.info("Dictionary:{}".format(dict))
        return dict

    def __dictionary_range_two_parameters(self, start, stop):
        """

        :param start: parameter 'from' for range()
        :type: int
        :param stop: parameter 'to' for range()
        :type: int
        :return: dict
        """
        dict = {key: key ** 2 for key in range(start, stop + 1)}
        self.log.info("Dictionary:{}".format(dict))
        return dict

    def __dictionary_range_three_parameters(self, start, stop, step):
        """

        :param start: parameter 'from' for range()
        :type: int
        :param stop: parameter 'to' for range()
        :type: int
        :param step: parameter 'step' for range()
        :type: int
        :return: dict
        """
        dict = {key: key ** 2 for key in range(start, stop + 1, step)}
        self.log.info("Dictionary:{}".format(dict))
        return dict
