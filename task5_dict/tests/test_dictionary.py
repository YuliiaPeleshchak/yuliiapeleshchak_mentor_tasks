import logging
from unittest import TestCase

from logger import logger
from task5_dict.dictionary import Dictionary


class TestDictionary(TestCase):
    """
    TestDictionary provides test methods for class Dictionary.
    """

    def setUp(self):
        logging.disable(logging.INFO)
        logging.disable(logging.ERROR)
        self.log = logger
        self.dict = Dictionary(self.log)

    def test_create_dictionary_success(self):
        expected_dict_1 = {
            1: 1,
            2: 4,
            3: 9,
            4: 16,
            5: 25,
            6: 36,
            7: 49,
            8: 64,
            9: 81,
            10: 100,
        }
        expected_dict_2 = {3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81, 10: 100}
        expected_dict_3 = {3: 9, 5: 25, 7: 49, 9: 81}
        start = 3
        stop = 10
        step = 2
        self.assertEqual(self.dict.dictionary(stop), expected_dict_1)
        self.assertEqual(self.dict.dictionary(start, stop), expected_dict_2)
        self.assertEqual(self.dict.dictionary(start, stop, step), expected_dict_3)

    def test_create_fail(self):
        start = 3
        stop = 10
        step = 2
        invalid_value = "7"
        with self.assertRaises(TypeError) as error:
            self.dict.dictionary(invalid_value)
        self.assertEqual(
            error.exception.args[0], f"Invalid data. Parameters must be integer type."
        )
        with self.assertRaises(TypeError) as error:
            self.dict.dictionary(start, invalid_value)
        self.assertEqual(
            error.exception.args[0], f"Invalid data. Parameters must be integer type."
        )
        with self.assertRaises(TypeError) as error:
            self.dict.dictionary(start, stop, invalid_value)
        self.assertEqual(
            error.exception.args[0], f"Invalid data. Parameters must be integer type."
        )
        with self.assertRaises(Exception) as error:
            self.dict.dictionary(start, stop, step, invalid_value)
        self.assertEqual(
            error.exception.args[0],
            f"Invalid data. You should pass one, two or three parameters.",
        )
