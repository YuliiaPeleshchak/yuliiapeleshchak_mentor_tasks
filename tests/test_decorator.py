import unittest

from utils import check_list_element_type, chek_data_type


class TestDecoratorCheckDataType(unittest.TestCase):
    """
       TestDecorator provides test methods for decorator_chek_type.
    """

    def test_success_single_type(self):
        value_str = "test_string"

        @chek_data_type((str,))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, value_str), "expected_result")

    def test_success_two_types(self):
        value_str = "test_string"
        value_int = 2

        @chek_data_type((str, int))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, value_str), "expected_result")
        self.assertEqual(test(self, value_int), "expected_result")

    def test_failure(self):
        value_wrong = "test_string"
        value_good = 5

        @chek_data_type((list, int))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, value_good), "expected_result")
        with self.assertRaises(TypeError) as error:
            test(self, value_wrong)
        self.assertEqual(error.exception.args[0], "Invalid data type.")


class TestDecoratorCheckListElementsType(unittest.TestCase):
    """
       TestDecorator provides test methods for decorator_chek_type.
    """

    def test_success_single_type(self):
        array = ["test_string_1", "test_string_2"]

        @check_list_element_type((str,))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, array), "expected_result")

    def test_success_two_types(self):
        array = ["test_string_1", "test_string_2", 2, 4]

        @check_list_element_type((str, int))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, array), "expected_result")

    def test_failure(self):
        array_wrong = ["test_string_1", "test_string_2", 2, 4]
        array_good = [1, 2, 5.6]

        @check_list_element_type((float, int))
        def test(self, value):
            return "expected_result"

        self.assertEqual(test(self, array_good), "expected_result")
        with self.assertRaises(TypeError) as error:
            test(self, array_wrong)
        self.assertEqual(error.exception.args[0], "Invalid data type.")
